package com.exercise.ryanair;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.exercise.ryanair.client.FinalRoutesTemplate;
import com.exercise.ryanair.client.FlightsTemplate;
import com.exercise.ryanair.client.RoutesTemplate;
import com.exercise.ryanair.model.Days;
import com.exercise.ryanair.model.Flights;
import com.exercise.ryanair.model.FlightsAtDay;
import com.exercise.ryanair.model.FlightsRoutes;
import com.exercise.ryanair.model.Routes;
import com.exercise.ryanair.service.FlightsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RyanairApplicationTests {

	private FlightsTemplate mockFlights;
	private RoutesTemplate mockRoutes;
	
	@Before
	public void setUp() {
		mockFlights = mock(FlightsTemplate.class);
		mockRoutes = mock(RoutesTemplate.class);

	}
	
	@Test
	   /**
     * Test the not-empty list with the execution of the process
     *
     * @return
     * @throws DirectGatewayException
     */
    public void shouldReturnProductsWithDuplicated() throws ParseException {

		FlightsService retrieveFlightInformation = new FlightsService();
		Mockito.when(mockRoutes.getRoutes()).thenReturn(mockListRoutes());
		Mockito.when(mockFlights.getFlightsAtDay("DUB", "WRO", "2019-06-01T07:00", "2019-06-01T23:59")).thenReturn(mockFlightsAtDay());
		
        final ArrayList<FlightsRoutes> finalRoutesTemplateResponse = retrieveFlightInformation.getFinalRoutes("DUB", "WRO", "2019-06-01T07:00", "2019-06-03T21:00");
        
        assertNotNull(finalRoutesTemplateResponse);
//        assertEquals();
    }

	
	//Ruta directa
    private Routes mockRoute1() {
    	Routes route = new Routes();
    	route.setAirportFrom("DUB");
    	route.setAirportTo("WRO");
    	route.setConnectingAirport("null");
    	route.setNewRoute("false");
    	route.setSeasonalRoute("false");
    	route.setOperator("RYANAIR");
    	route.setGroup("ETHNIC");
		return route;
    }
    
	//Rutas no directas 1
    private Routes mockRoute2() {
    	Routes route = new Routes();
    	route.setAirportFrom("STN");
    	route.setAirportTo("WRO");
    	route.setConnectingAirport("null");
    	route.setNewRoute("false");
    	route.setSeasonalRoute("false");
    	route.setOperator("RYANAIR");
    	route.setGroup("ETHNIC");
    	return route;
    }
    
    //Rutas no directas 2
    private Routes mockRoute3() {
    	Routes route = new Routes();
    	route.setAirportFrom("DUB");
    	route.setAirportTo("STN");
    	route.setConnectingAirport("null");
    	route.setNewRoute("false");
    	route.setSeasonalRoute("false");
    	route.setOperator("RYANAIR");
    	route.setGroup("ETHNIC");
    	return route;
    }
    
    private Routes[] mockListRoutes(){

    	Routes [] routes = new Routes [3];
    	routes[0] = mockRoute1();
    	routes[1] = mockRoute2();
    	routes[2] = mockRoute3();
    	
    	
		return routes;
    }
    
    //Info ruta directa 1
    private Flights mockFlights1() {
    	Flights flights = new Flights();
    	flights.setNumber("1926");
    	flights.setArrivalTime("23:55");
    	flights.setDepartureTime("21:35");
		return flights;
    }
    
    //Info ruta no directa 1
    private Flights mockFlights2() {
    	Flights flights = new Flights();
    	flights.setNumber("1926");
    	flights.setArrivalTime("09:00");
    	flights.setDepartureTime("11:15");
		return flights;
    }
    
    //Info ruta no directa 1
    private Flights mockFlights3() {
    	Flights flights = new Flights();
    	flights.setNumber("1926");
    	flights.setArrivalTime("15:15");
    	flights.setDepartureTime("18:35");
		return flights;
    }
    
    private Days mockDays1() {
    	Days days = new Days();
    	days.setDay("1");
    	List<Flights> flights = new ArrayList<Flights>();
    	flights.add(0, mockFlights1());
    	flights.add(1, mockFlights2());
    	days.setFlights(flights);
		return days;
    }
    
    private Days mockDays2() {
    	Days days = new Days();
    	days.setDay("2");
    	List<Flights> flights = new ArrayList<Flights>();
    	flights.add(mockFlights1());
    	flights.add(mockFlights2());
    	days.setFlights(flights);
		return days;
    }
    
    
    private FlightsAtDay mockFlightsAtDay() {
    	FlightsAtDay flightsAtDay = new FlightsAtDay();
    	flightsAtDay.setMonth("6");
    	List<Days> days = new ArrayList<Days>();
    	
    	days.add(0,mockDays1());
    	days.add(1,mockDays2());
    	
//    	days.add(mockDays1());
//    	days.add(mockDays2());
    	flightsAtDay.setDays(days);
		return flightsAtDay;
    }
}
