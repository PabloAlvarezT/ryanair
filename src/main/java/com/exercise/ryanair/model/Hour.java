package com.exercise.ryanair.model;

import java.time.LocalTime;

public class Hour{
	
	LocalTime departureHour;
	LocalTime arriveHour;	
	
	public Hour() {
		super();
	}
	public Hour(LocalTime departureHour, LocalTime arriveHour) {
		
		super();
		this.departureHour = departureHour;
		this.arriveHour = arriveHour;
	}
	public LocalTime getDepartureHour() {
		return departureHour;
	}
	public void setDepartureHour(LocalTime departureHour) {
		this.departureHour = departureHour;
	}
	public LocalTime getArriveHour() {
		return arriveHour;
	}
	public void setArriveHour(LocalTime arriveHour) {
		this.arriveHour = arriveHour;
	}
}