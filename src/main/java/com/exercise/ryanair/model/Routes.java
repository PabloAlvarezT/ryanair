package com.exercise.ryanair.model;

import java.util.List;

public class Routes{
	
	String airportFrom;
	String airportTo;	
	String connectingAirport;
	String newRoute;
	String seasonalRoute;
	String operator;
	String group;
	String carrierCode;
	List<String> similarArrivalAirportCodes;
	List<String> tags;
	
	public Routes() {
		super();
	}
	public Routes(	String airportFrom, String airportTo, String connectingAirport, String newRoute, String seasonalRoute, String operator, 
			String group, String carrierCode, List<String> similarArrivalAirportCodes, List<String> tags) {
		
		super();
		this.airportFrom = airportFrom;
		this.airportTo = airportTo;
		this.connectingAirport = connectingAirport;
		this.newRoute = newRoute;
		this.seasonalRoute = seasonalRoute;
		this.operator = operator;
		this.group = group;
		this.carrierCode = carrierCode;
		this.similarArrivalAirportCodes = similarArrivalAirportCodes;
		this.tags = tags;
		
	}
	public String getAirportFrom() {
		return airportFrom;
	}
	public void setAirportFrom(String airportFrom) {
		this.airportFrom = airportFrom;
	}
	public String getAirportTo() {
		return airportTo;
	}
	public void setAirportTo(String airportTo) {
		this.airportTo = airportTo;
	}
	public String getConnectingAirport() {
		return connectingAirport;
	}
	public void setConnectingAirport(String connectingAirport) {
		this.connectingAirport = connectingAirport;
	}
	public String getNewRoute() {
		return newRoute;
	}
	public void setNewRoute(String newRoute) {
		this.newRoute = newRoute;
	}
	public String getSeasonalRoute() {
		return seasonalRoute;
	}
	public void setSeasonalRoute(String seasonalRoute) {
		this.seasonalRoute = seasonalRoute;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getCarrierCode() {
		return carrierCode;
	}
	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}
	public List<String> getSimilarArrivalAirportCodes() {
		return similarArrivalAirportCodes;
	}
	public void setSimilarArrivalAirportCodes(List<String> similarArrivalAirportCodes) {
		this.similarArrivalAirportCodes = similarArrivalAirportCodes;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	
}