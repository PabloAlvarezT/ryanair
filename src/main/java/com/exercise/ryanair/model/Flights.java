package com.exercise.ryanair.model;

public class Flights{
	
	
	String number;
	String departureTime;
	String arrivalTime;
	
	
	public Flights() {
		super();
	}
	public Flights(	String number, String departureTime, String arrivalTime) {
		
		super();
		this.number = number;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	
}