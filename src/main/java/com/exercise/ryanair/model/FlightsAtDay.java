package com.exercise.ryanair.model;

import java.util.ArrayList;
import java.util.List;

public class FlightsAtDay{
	
	String month;
	List<Days> days;	
	
	public FlightsAtDay() {
		super();
	}
	public FlightsAtDay(	String month, ArrayList<Days> days) {
		
		super();
		this.month = month;
		this.days = days;
		
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public List<Days> getDays() {
		return days;
	}
	public void setDays(List<Days> days) {
		this.days = days;
	}
	
}