package com.exercise.ryanair.model;

import java.util.ArrayList;
import java.util.List;

public class Days{
	
	String day;
	List<Flights> flights;	
	
	public Days() {
		super();
	}
	public Days(	String day, ArrayList<Flights> flights) {
		
		super();
		this.day = day;
		this.flights = flights;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public List<Flights> getFlights() {
		return flights;
	}
	public void setFlights(List<Flights> flights) {
		this.flights = flights;
	}
	
}