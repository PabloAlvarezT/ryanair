package com.exercise.ryanair.model;

import java.util.ArrayList;

public class FlightsRoutes{
	
	String stops;
	ArrayList<Legs> legs;	
	
	public FlightsRoutes() {
		super();
	}
	public FlightsRoutes(	String stops, ArrayList<Legs> legs) {
		
		super();
		this.stops = stops;
		this.legs = legs;
	}
	public String getStops() {
		return stops;
	}
	public void setStops(String stops) {
		this.stops = stops;
	}
	public ArrayList<Legs> getLegs() {
		return legs;
	}
	public void setLegs(ArrayList<Legs> legs) {
		this.legs = legs;
	}
	
}