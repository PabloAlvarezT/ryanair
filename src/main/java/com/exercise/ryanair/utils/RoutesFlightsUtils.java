package com.exercise.ryanair.utils;
 
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.exercise.ryanair.model.Days;
import com.exercise.ryanair.model.Flights;
import com.exercise.ryanair.model.FlightsAtDay;
import com.exercise.ryanair.model.FlightsRoutes;
import com.exercise.ryanair.model.Hour;
import com.exercise.ryanair.model.Legs;

public class RoutesFlightsUtils {

	public static LocalTime endHour = LocalTime.parse("23:59");	
	
	/*
	 * Form the flight's legs from information (arrival, arrivaldate...)
	 */
	
	public static ArrayList<Legs> formLegs(String arrival, String arrivalDateTime, String departure, String departureDateTime,
			 ArrayList<Legs> legs) throws ParseException {

		Legs leg = new Legs();
		leg.setArrivalAirport(arrival);
		leg.setArrivalDateTime(arrivalDateTime);
		leg.setDepartureAirport(departure);
		leg.setDepartureDateTime(departureDateTime);
		legs.add(leg);
		return legs;
	}
		
	/*
	 * Form the flight's final form from the information, the legs and the stops (1 or 0)
	 */
	
	public static ArrayList<FlightsRoutes> fillFinalRoute(String arrival, String arrivalDateTime, String departure, String departureDateTime,
			ArrayList finalRoutes, ArrayList<Legs> legs, String stops) throws ParseException {
		
		FlightsRoutes flightsRoutes = new FlightsRoutes();
		ArrayList finalRoutesOrdered = new ArrayList();
		flightsRoutes.setLegs(legs);
		flightsRoutes.setStops(stops);
		finalRoutes.add(flightsRoutes);
		deleteDuplicated(finalRoutes);
		return finalRoutes;
	}
	
	/*
	 * Add all the final flight information to the final arraylist
	 */
	
	private static  ArrayList<FlightsRoutes> deleteDuplicated (ArrayList finalRoutes){
        HashSet hs = new HashSet();
        hs.addAll(finalRoutes);
        finalRoutes.clear();
        finalRoutes.addAll(hs);
		return finalRoutes;
		
	}
	
	/*
	 * Check the difference (2 hours) between the localtimes of the flights
	 */
	
		public static Boolean twoHoursDifference (String dateTimeDeparture, String dateTimeArrival) throws ParseException {
			boolean isTwoHoursGreater = false;
			Hour hour = new Hour();
			hour = formatHour(dateTimeDeparture, dateTimeArrival);
			//Si la salida del 2� vuelo es 2h mayor que la llegada del primer vuelo
			//Falta contemplar que el vuelo siguiente está fuera del día
			if ((hour.getArriveHour().plus(2, ChronoUnit.HOURS) != null) && 
					(hour.getDepartureHour().compareTo(hour.getArriveHour().plus(2, ChronoUnit.HOURS)))> 0){
				isTwoHoursGreater = true;
				//Si la hora de llegada del vuelo 1 + 2h es mayor que la hora de salida del vuelo 2
			}
			return isTwoHoursGreater;
		}
		
		/*
		 * Check if the day of the flight is between the given range from the endpoint
		 */
		
		public static Boolean dayIsBetween (String departureDateTime, String day, String arrivalDateTime, String day2) throws ParseException {
			boolean isBetween = false;
			String departureDay = extractDayFromString(departureDateTime);
			String arrivalDay = extractDayFromString(arrivalDateTime);
			if ((Integer.parseInt(day) >= Integer.parseInt(departureDay)) && (Integer.parseInt(day2) <= Integer.parseInt(arrivalDay)) &&
					(Integer.parseInt(day) <= Integer.parseInt(day2))) {
				isBetween = true;
			}
			return isBetween;
		}

		/*
		 * Form the date from the arrivaldatetime, the flight day and the arrivalime
		 */
		
		public static String formDirectRouteDate(String arrivalDateTime, String day, String arrivalTime) {
			String[] parts = arrivalDateTime.split("-");
			String anio = parts[0]; // 2018
			String mes = parts[1]; // 03
			String arrivalFinalTime = (anio + "-" + mes + "-" + day + "T" + arrivalTime );
			return arrivalFinalTime;
			
		}
		
		/*
		 * Extract the date without the hour
		 */
		
		private static String extractDayFromString(String date) {
			String[] parts = date.split("-");
			String anio = parts[0]; // 2018
			String mes = parts[1]; // 03
			String dia1 = parts[2]; // 01T07:00
			String[] parts2 = parts[2].split("T");
			return dia1 = parts2[0];
		}
		
		
		/*
		 * Form the string date from all the date parameters and the hour
		 */
		
		private static String formatDateWithHour (String dateTime, String hour) throws ParseException {
			String monthParameter = RoutesFlightsUtils.dateMonth(RoutesFlightsUtils.formatDateString(dateTime));
			String yearParameter = RoutesFlightsUtils.dateYear(RoutesFlightsUtils.formatDateString(dateTime));
			String dayParameter = RoutesFlightsUtils.dateDay(RoutesFlightsUtils.formatDateString(dateTime));
			String dateStartTime = (yearParameter + "-" + monthParameter + "-" + dayParameter + "T" + hour);
			return dateStartTime;
		}
		
		/*
		 * Form the string date from all the date parameters and the day and hour
		 */
		
		public static String formatDateCustomDay (String dateTime, String day, String hour) throws ParseException {
			String monthParameter = RoutesFlightsUtils.dateMonth(RoutesFlightsUtils.formatDateString(dateTime));
			String yearParameter = RoutesFlightsUtils.dateYear(RoutesFlightsUtils.formatDateString(dateTime));
			String dateStartTime = (yearParameter + "-" + monthParameter + "-" + day + "T" + hour);
			return dateStartTime;
		}
		
		/*
		 * Filter the flights by day
		 */
		
		private static List<FlightsAtDay> filterListByFlightDay(List<FlightsAtDay> flightsFilter, String dayParameter) {

			ArrayList<FlightsAtDay> flights= null; 
			ArrayList<Days> days = null;
			FlightsAtDay flightsAtDayAux = new FlightsAtDay();

			for (FlightsAtDay flightFilter : flightsFilter) {
				for(Days day : flightFilter.getDays()) {
					if (dayParameter.equals(day.getDay())) {
						Days dayAux = new Days();
						dayAux.setDay(dayParameter);
						dayAux.setFlights(day.getFlights());
						days.add(dayAux);
						flightsAtDayAux.setDays(days);
						flightsAtDayAux.setMonth(flightFilter.getMonth());
					}
				}
			}
			List<FlightsAtDay> flightsFinal = Arrays.asList(flightsAtDayAux);
			return flightsFinal;
		}
	
	/*
	 * Convierte la fecha 2018-03-01T07:00 en fecha con formato Date
	 * 
	 */
	public static Date formatDateString(String date) throws ParseException {
		//DepartureDateTime entra en 2018-03-01T07:00
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String[] parts = date.split("-");
		String anio = parts[0]; // 2018
		String mes = parts[1]; // 03
		String dia = parts[2]; // 01T07:00
		String[] parts2 = parts[2].split("T");
		dia = parts2[0]; // 01
		String hora = parts2[1]; // 07:00
		String fechaFinal=(anio + "-" + mes + "-" + dia + " " + hora + ":00");
		Date fechaConHora = sdf.parse(fechaFinal);
		return fechaConHora;
	}
	
	/*
	 * Extrae el anio de la fecha
	 */
	public static String dateYear(Date date) throws ParseException {
		String formato="yyyy";
		DateFormat dateYearFormatted = new SimpleDateFormat(formato);
		return dateYearFormatted.format(date);
	}

	/*
	 * Extrae el mes de la fecha
	 */
	public static String dateMonth(Date date) throws ParseException {
		String formato="MM";
		DateFormat dateMonthFormatted = new SimpleDateFormat(formato);
		return dateMonthFormatted.format(date);
	}

	/*
	 * Extrae el d�a de la fecha
	 */
	public static String dateDay(Date date) throws ParseException {
		String formato="dd";
		DateFormat dateDayFormatted = new SimpleDateFormat(formato);
		return dateDayFormatted.format(date);
	}
	
	/*
	 * Extrae la hora de la fecha
	 */
	
	public static String extractHour(String date) throws ParseException {
		//DepartureDateTime entra en 2018-03-01T07:00
		String[] parts = date.split("T");
		String hora = parts[1]; // 07:00
		return hora;
	}
	
	/*
	 * Extrae la fecha sin la hora
	 */
	
	public static String extractStringDate(String date) throws ParseException {
		//DepartureDateTime entra en 2018-03-01T07:00
		String[] parts = date.split("T");
		String fecha = parts[0]; // 2018-03-01
			String[] fechaSplit = fecha.split("-");
			String anio = fechaSplit[0];
			String mes = fechaSplit[1];
			String dia = fechaSplit[2];
			fecha = (anio + "-" + mes + "-" + dia);
		return fecha;
	}

	/*
	 * Forma fecha con hora
	 */
	
	public static Hour formatHour(String departureDateTime, String arrivalDateTime) throws ParseException {
		Hour hour = new Hour();
		hour.setArriveHour(LocalTime.parse(arrivalDateTime));
		hour.setDepartureHour(LocalTime.parse(departureDateTime));
		return hour;
	}
	
	/*
	 * Forma fecha con hora
	 */
	
	public static Hour formatHourForRoutes(String departureDateTime, String arrivalDateTime) throws ParseException {
		Hour hour = new Hour();
		hour.setArriveHour(LocalTime.parse(extractHour(arrivalDateTime)));
		hour.setDepartureHour(LocalTime.parse(extractHour(departureDateTime)));
		return hour;
	}
}
 