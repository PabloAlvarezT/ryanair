package com.exercise.ryanair.client;
 
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.exercise.ryanair.model.Days;
import com.exercise.ryanair.model.FlightsAtDay;
import com.exercise.ryanair.utils.RoutesFlightsUtils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class FlightsTemplate {
 
private static FlightsAtDay flightsAtDay;

public FlightsAtDay getFlightsAtDay(String departure, String arrival, String departureDateTime, String arrivalDateTime)  throws ParseException {
 
  //ENDPOINT: http://<HOST>/<CONTEXT>/interconnections?departure={departure}&arrival={arrival}&departureDateTime={departureDateTime}&arrivalDateTime={arrivalDateTime}
  //URL2: https://services-api.ryanair.com/timtbl/3/schedules/DUB/WRO/years/2019/months/6
  //DepartureDateTime entra en 2018-03-01T07:00
	
  RestTemplate restTemplate = new RestTemplate();
  
  
  String monthParameter = (RoutesFlightsUtils.dateMonth(RoutesFlightsUtils.formatDateString(departureDateTime))).replace("0", "");
  String yearParameter = RoutesFlightsUtils.dateYear(RoutesFlightsUtils.formatDateString(departureDateTime));
  String dayParameter = RoutesFlightsUtils.dateDay(RoutesFlightsUtils.formatDateString(departureDateTime));

  String connectionUrl = "https://services-api.ryanair.com/timtbl/3/schedules/" + departure 
		  + "/" + arrival + "/years/" + yearParameter + "/months/" + monthParameter;
  try {
	  ResponseEntity<FlightsAtDay> responseEntity = restTemplate.getForEntity(connectionUrl, FlightsAtDay.class);
	  if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
		  flightsAtDay = responseEntity.getBody();
	  }
  } catch (Exception e){
	  System.out.println("Error:");
	  System.out.println("Salida: " + departure);
	  System.out.println("Llegada: " + arrival);
	  System.out.println("Anio: " + yearParameter);
	  System.out.println("Mes: " + monthParameter);
	  System.out.println("Mensaje de error: " + e.getMessage());
	  e.printStackTrace();
	  
  }

//  if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
//	  //RoutesDto routesDto= responseEntity.getBody();
//	  flightsAtDay = responseEntity.getBody();
//  }

  
  List<FlightsAtDay> flightslist = Arrays.asList(flightsAtDay);
  List<FlightsAtDay> flightsFilter = flightslist.stream()
  .filter(flightAtDay-> flightAtDay.getMonth()==monthParameter)
  .collect(Collectors.toList());
  
  //flightsFilter lista que concuerda con un mes concreto
  filterListByFlightDay(flightsFilter, dayParameter);
  
  
  return flightsAtDay;
  
 }

private static List<FlightsAtDay> filterListByFlightDay(List<FlightsAtDay> flightsFilter, String dayParameter) {

	ArrayList<FlightsAtDay> flights= null; 
	ArrayList<Days> days = null;
	FlightsAtDay flightsAtDayAux = new FlightsAtDay();
	
	for (FlightsAtDay flightFilter : flightsFilter) {
		for(Days day : flightFilter.getDays()) {
			if (dayParameter.equals(day.getDay())) {
				Days dayAux = new Days();
				dayAux.setDay(dayParameter);
				dayAux.setFlights(day.getFlights());
				days.add(dayAux);
				flightsAtDayAux.setDays(days);
				flightsAtDayAux.setMonth(flightFilter.getMonth());
			}
		}
	}
	List<FlightsAtDay> flightsFinal = Arrays.asList(flightsAtDayAux);
	return flightsFinal;
	
}



}
 