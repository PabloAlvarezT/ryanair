package com.exercise.ryanair.client;
 
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.exercise.ryanair.model.Routes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RoutesTemplate {
 
private static Routes[] routes;

public Routes[] getRoutes() {
 
  //ENDPOINT: http://<HOST>/<CONTEXT>/interconnections?departure={departure}&arrival={arrival}&departureDateTime={departureDateTime}&arrivalDateTime={arrivalDateTime}
  //URL: https://services-api.ryanair.com/timtbl/3/schedules/DUB/WRO/years/2019/months/3 
  //Only Routes with operator=RYANAIR and connectingAirport = null, we filter after getting the information
	
  RestTemplate restTemplate = new RestTemplate();
  ResponseEntity<Routes[]> responseEntity = restTemplate.getForEntity("https://services-api.ryanair.com/locate/3/routes", Routes[].class);

  if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
	  routes = responseEntity.getBody();
  }
  
  List<Routes> routeslist = Arrays.asList(routes);
  List<Routes> routesFilter = routeslist.stream()
	 .filter(route-> route.getConnectingAirport()==null && "RYANAIR".equals(route.getOperator()))
	 .collect(Collectors.toList());
  
  //filterListRoutes(routes);
  return routes;
  
 }

}
 