package com.exercise.ryanair.client;

import java.text.ParseException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.exercise.ryanair.model.Days;
import com.exercise.ryanair.model.Flights;
import com.exercise.ryanair.model.FlightsAtDay;
import com.exercise.ryanair.model.FlightsRoutes;
import com.exercise.ryanair.model.Hour;
import com.exercise.ryanair.model.Legs;
import com.exercise.ryanair.model.Routes;
import com.exercise.ryanair.utils.RoutesFlightsUtils;

public class FinalRoutesTemplate {

	private static com.exercise.ryanair.model.FlightsAtDay[] flightsAtDay;
	private static String noStop = "0";
	private static String oneStop = "1";

	public static ArrayList<FlightsRoutes> getFinalRoutes(Routes[] routes, FlightsAtDay flightsAtDay, String departure, 
			String arrival, String departureDateTime, String arrivalDateTime) throws ParseException {

		ArrayList<FlightsRoutes> flightsFinals = null;
		flightsFinals = directFlights(routes, flightsAtDay, departure, arrival, departureDateTime, arrivalDateTime);
		return flightsFinals;

	}

	private static ArrayList<FlightsRoutes> directFlights(Routes[] routes, FlightsAtDay flightsAtDay, String departure, 
			String arrival, String departureDateTime, String arrivalDateTime) throws ParseException {

		ArrayList<FlightsRoutes> finalRoutes = new ArrayList<FlightsRoutes>();
		Hour hour, hourFlight = new Hour();
		FlightsTemplate flightsTemplate = new FlightsTemplate();
		Boolean nextDay = false;
		
		//Vuelos directos
		for (Routes route : routes) {
			if ((departure.equals(route.getAirportFrom())) && (arrival.equals(route.getAirportTo()))){
					for (Days day : flightsAtDay.getDays()) {
						for (Flights flight : day.getFlights()) {
							hourFlight.setArriveHour(LocalTime.parse(flight.getArrivalTime()));
							hourFlight.setDepartureHour(LocalTime.parse(flight.getDepartureTime()));
							hour = RoutesFlightsUtils.formatHourForRoutes(departureDateTime, arrivalDateTime);
							if (hourFlight.getDepartureHour().compareTo(hour.getDepartureHour()) >= 0) {
								ArrayList<Legs> legs = new ArrayList<Legs>();
								legs = RoutesFlightsUtils.formLegs(arrival, 
										(RoutesFlightsUtils.formDirectRouteDate(arrivalDateTime, day.getDay(), flight.getArrivalTime())), 
										departure, 
										(RoutesFlightsUtils.formDirectRouteDate(arrivalDateTime, day.getDay(), flight.getDepartureTime())), legs);
								finalRoutes = RoutesFlightsUtils.fillFinalRoute(arrival, 
										(RoutesFlightsUtils.formDirectRouteDate(arrivalDateTime, day.getDay(), flight.getArrivalTime())), departure, 
										(RoutesFlightsUtils.formDirectRouteDate(arrivalDateTime, day.getDay(), flight.getDepartureTime())), finalRoutes, legs, noStop);
							}
						}
					}
			} 
		}
		
		//Vuelos no directos con salida departure
		List<Routes> routeslist = Arrays.asList(routes);
		List<Routes> routesWithDeparture = routeslist.stream()
			.filter(routeFinalNoConnection-> departure.equals(routeFinalNoConnection.getAirportFrom()))
			.collect(Collectors.toList());
		
		//Vuelos no directos con llegada arrival
		List<Routes> routeslist2 = Arrays.asList(routes);
		List<Routes> routesWithArrival = routeslist2.stream()
			.filter(routeFinalNoConnection-> arrival.equals(routeFinalNoConnection.getAirportTo()))
			.collect(Collectors.toList());
		
		FlightsAtDay flightOrigen = null;
		FlightsAtDay flightDestino = null;

				for (Routes routeFinalNoConnection : routesWithDeparture) { //Por cada ruta conectada con salida(Vuelo 1)
					for (Routes routeFinalNoConnection2 : routesWithArrival) { //Comparo con las demas con llegada(Vuelo 2)
						if (routeFinalNoConnection2.getAirportFrom().equals(routeFinalNoConnection.getAirportTo())) {	//Si el origen del vuelo 2 es igual al destino del vuelo 1
							flightOrigen = flightsTemplate.getFlightsAtDay(routeFinalNoConnection.getAirportFrom(), routeFinalNoConnection.getAirportTo(), //Vuelo 1
									departureDateTime, arrivalDateTime);
							flightDestino = flightsTemplate.getFlightsAtDay(routeFinalNoConnection2.getAirportFrom(), routeFinalNoConnection2.getAirportTo(), //Vuelo 2 
									departureDateTime, arrivalDateTime);
							if (flightOrigen != null && flightDestino != null) {
								for (Days day : flightOrigen.getDays()) {
									for (Flights flight : day.getFlights()) { //Iterando en primera lista
											for (Days day2 : flightDestino.getDays()) {
												for (Flights flight2 : day2.getFlights()) { //Iterando en segunda lista 
													if (RoutesFlightsUtils.dayIsBetween(departureDateTime, day.getDay(), arrivalDateTime, day2.getDay())
															&& RoutesFlightsUtils.twoHoursDifference(flight2.getDepartureTime(), flight.getArrivalTime()))  { 
														ArrayList<Legs> legs = new ArrayList<Legs>();
														legs = RoutesFlightsUtils.formLegs(routeFinalNoConnection.getAirportTo(), 
																(RoutesFlightsUtils.formatDateCustomDay(departureDateTime, day.getDay(), flight.getArrivalTime())),
																routeFinalNoConnection.getAirportFrom(), 
																(RoutesFlightsUtils.formatDateCustomDay(departureDateTime, day.getDay(), flight.getDepartureTime()))
																, legs);
														legs = RoutesFlightsUtils.formLegs(routeFinalNoConnection2.getAirportTo(), 
																(RoutesFlightsUtils.formatDateCustomDay(departureDateTime, day2.getDay(), flight2.getArrivalTime())),
																routeFinalNoConnection2.getAirportFrom(), 
																(RoutesFlightsUtils.formatDateCustomDay(departureDateTime, day2.getDay(), flight2.getDepartureTime())),
																legs);
														finalRoutes = RoutesFlightsUtils.fillFinalRoute(arrival, arrivalDateTime, departure, departureDateTime, finalRoutes, legs, oneStop);
													}
												}
												}
											}
										}
							}
						}
			}
			}
				return finalRoutes;
		}


	
	

	}
