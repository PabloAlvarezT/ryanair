package com.exercise.ryanair.controller;

import java.text.ParseException;
import java.util.ArrayList;
import com.exercise.ryanair.model.FlightsRoutes;
import com.exercise.ryanair.service.FlightsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
 * Author: Pablo Alvarez
 */
@RestController
public class FlightsController {

	//ENDPOINT: http://<HOST>/<CONTEXT>/interconnections?departure={departure}&arrival={arrival}&departureDateTime={departureDateTime}&arrivalDateTime={arrivalDateTime}
	//URL: https://services-api.ryanair.com/timtbl/3/schedules/DUB/WRO/years/2019/months/3 
	@Autowired
	FlightsService flightsService;

	@RequestMapping(value = "/interconnections/{departure}/{arrival}/{departureDateTime}/{arrivalDateTime}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ArrayList<FlightsRoutes> getFlights(@PathVariable String departure, @PathVariable String arrival, @PathVariable String departureDateTime, @PathVariable String arrivalDateTime) 
			throws ParseException {
		return flightsService.getFinalRoutes(departure, arrival, departureDateTime, arrivalDateTime);
	}

}
