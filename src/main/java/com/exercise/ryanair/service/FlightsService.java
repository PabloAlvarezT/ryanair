package com.exercise.ryanair.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.exercise.ryanair.client.FinalRoutesTemplate;
import com.exercise.ryanair.client.FlightsTemplate;
import com.exercise.ryanair.client.RoutesTemplate;
import com.exercise.ryanair.model.FlightsAtDay;
import com.exercise.ryanair.model.FlightsRoutes;
import com.exercise.ryanair.model.Routes;
import org.springframework.stereotype.Service;

@Service
public class FlightsService {

 public FlightsService() {
  super();
 }

 private static FlightsAtDay flightsAtDay;
 private static Routes[] routes;
 private static ArrayList<FlightsRoutes> finalRoutes;
 
public Routes[] getRoutes(String departure, String arrival, String departureDateTime, String arrivalDateTime) {

	
	RoutesTemplate routesTemplate = new RoutesTemplate();
	routes = routesTemplate.getRoutes();
	return routes;
}

public FlightsAtDay getFlightsAtDay(String departure, String arrival, String departureDateTime, String arrivalDateTime) throws ParseException {
	
	FlightsTemplate flightsTemplate = new FlightsTemplate();
	flightsAtDay = flightsTemplate.getFlightsAtDay(departure, arrival, departureDateTime, arrivalDateTime);

	return flightsAtDay;
}

public ArrayList<FlightsRoutes> getFinalRoutes(String departure, String arrival, String departureDateTime,
		String arrivalDateTime) throws ParseException {
	FinalRoutesTemplate finalRoutesTemplate = new FinalRoutesTemplate();
	finalRoutes = finalRoutesTemplate.getFinalRoutes(getRoutes(departure, arrival, departureDateTime, arrivalDateTime),
			getFlightsAtDay(departure, arrival, departureDateTime, arrivalDateTime), departure, arrival, departureDateTime, arrivalDateTime);

	return finalRoutes;
}



}

